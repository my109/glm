# Generalized Linear Models in R

These materials accompany the DUPRI "Generalized Linear Models in R" training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* glm.Rmd - Rmarkdown HTML version of training code
* glm.html - HTML report of training for reference
* beer.dta - Data set used for workshop
* gss2022.dta - Data set used for workshop
* Big Data SQL.Rproj - RStudio project file
